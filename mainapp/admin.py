from django.contrib import admin
from mainapp.models import Menu
from django.core.exceptions import ValidationError

class MyMenuFilter(admin.SimpleListFilter):
    title = 'Parent filter'
    parameter_name = 'parent'
    def lookups(self, request, model_admin):
        menu_list = [(menu.slug, menu.title) for menu in Menu.objects.filter(parent=None).all() ]
        return menu_list

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(parent__slug=self.value())

class MenuAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'parent', 'status', 'created_by', 'created', 'img')
    list_filter = ('status', 'created', MyMenuFilter)
    readonly_fields = ( 'created_by', 'created', 'updated', 'img')
    search_fields = ('title',)
    prepopulated_fields = { 'slug': ('title',)}
    save_on_top = True

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        if obj.parent != obj:
            super().save_model(request, obj, form, change)
        return ValidationError("Связать себя невозможно!!!")


admin.site.register(Menu, MenuAdmin)