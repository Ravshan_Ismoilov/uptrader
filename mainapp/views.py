from django.shortcuts import render
from mainapp.models import Menu

def index(request, slug=None):
    menu = Menu.active.filter(parent=None).all()
    return render(request, 'index.html', locals())