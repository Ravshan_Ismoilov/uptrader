from django.db import models
from django.utils.html import mark_safe
from django.contrib.auth import get_user_model

User = get_user_model()


STATUS_CHOICES = (
	('active', 'Active'),
	('arxiv', 'Arxiv'),
	('delete', 'Deleted')
)

class ActiveManager(models.Manager):
	def get_queryset(self):
		return super().get_queryset().filter(status='active')

class Menu(models.Model):
    parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='sub_menu', null=True, blank=True, verbose_name="Sub Menu")
    title = models.CharField("Menu", max_length=255)
    slug = models.SlugField(unique=True,)
    image = models.ImageField(upload_to="menu/%Y/%m/%d/", null=True, blank=True, verbose_name="Photo")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="related_user_menu", verbose_name="Created by")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Created")
    updated = models.DateTimeField(auto_now=True, verbose_name="Updated")
    objects = models.Manager()
    active = ActiveManager()

    class Meta:
        verbose_name = "Menu"
        verbose_name_plural = "Menu"
    
    def img(self):
        return mark_safe('\
				<a href="/media/{photo}" target="blank" alt="{name}"> \
					<img src="/media/{photo}" width="50" height="50" /> \
				</a>'.format(
					photo=self.image,
					name=self.title
				)
			)
    img.short_description = "Photo"

    def __str__(self):
        full_path = [self.title]
        k = self.parent
        while k is not None:
            full_path.append(k.title)
            k = k.parent
        return ' -> '.join(full_path[::-1]) 