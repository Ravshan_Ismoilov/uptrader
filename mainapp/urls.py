from django.urls import path
from mainapp.views import index

app_name = 'mainapp'

urlpatterns = [
    path('<slug:slug>/', index, name='index'),
    path('', index, name='index')
]
