from django import template
from mainapp.models import Menu

register = template.Library()

@register.inclusion_tag('menu.html')
def draw_menu(url=None, slug=None):

    if slug:
        menu =  Menu.active.filter(parent__slug=slug).all()
        
    return {'menu': menu, 'slug': url}